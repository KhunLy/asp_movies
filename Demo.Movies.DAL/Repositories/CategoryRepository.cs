﻿using Demo.Movies.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace Demo.Movies.DAL.Repositories
{
    public class CategoryRepository
    {
        // on déplacera plus tard la connection string dans la configuration de notre application
        private string connectionString = @"server=K-PC\SQLSERVER;initial catalog=Movies;integrated security=SSPI";

        /// <summary>
        /// Ajouter une categorie dans la table Category
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>l'id de la ligne qui a été insérée</returns>
        public int Add(Category entity)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                string query = "INSERT INTO [Category]([name], [description]) OUTPUT INSERTED.Id VALUES (@p1, @p2)";
                SqlCommand command = connection.CreateCommand();
                command.CommandText = query;
                command.Parameters.AddWithValue("p1", entity.Name);
                command.Parameters.AddWithValue("p2", (object)entity.Description ?? DBNull.Value);
                int id = (int)command.ExecuteScalar();
                return id;
            }
        }
    }
}
