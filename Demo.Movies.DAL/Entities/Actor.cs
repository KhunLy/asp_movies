﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demo.Movies.DAL.Entities
{
    public class Actor
    {
        public int Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime BirthDate { get; set; }
        public byte[] ImageFile { get; set; }
        public string ImageMimeType { get; set; }
    }
}
