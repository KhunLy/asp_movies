﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demo.Movies.DAL.Entities
{
    public class Movie
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Synopsis { get; set; }
        public int RealeaseYear { get; set; }
        public string PosterUrl { get; set; }
        public byte Rating { get; set; }
        public int CategoryId { get; set; }
    }
}
