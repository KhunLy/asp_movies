﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demo.Movies.DAL.Entities
{
    public class Casting
    {
        public int Id { get; set; }
        public int ActorId { get; set; }
        public int MovieId { get; set; }
    }
}
